#include "stdafx.h"
#include "NtlSfx.h"

#include "AuthServer.h"



//--------------------------------------------------------------------------------------//
//		Get the account ID and log in to Char Server									//
//--------------------------------------------------------------------------------------//
void CClientSession::SendCharLogInReq(CNtlPacket * pPacket, CAuthServer * app) 
{
	NTL_PRINT(PRINT_APP, "Client Received Login Request");

	sUA_LOGIN_REQ * req = (sUA_LOGIN_REQ *)pPacket->GetPacketData();

	CNtlPacket packet(sizeof(sAU_LOGIN_RES));
	sAU_LOGIN_RES * res = (sAU_LOGIN_RES *)packet.GetPacketData();
	
	CMasterSession* pMasterServer = app->AcquireServerSession();
	
	res->wOpCode = AU_LOGIN_RES;
	memcpy(res->awchUserId, req->awchUserId, NTL_MAX_SIZE_USERID_UNICODE);
	strcpy_s((char*)res->abyAuthKey, NTL_MAX_SIZE_AUTH_KEY, "Dbo");

	
	app->db->prepare("CALL AuthLogin (? ,?, @acc_id, @result_code)");
	app->db->setString(1, Ntl_WC2MB(req->awchUserId));
	app->db->setString(2, Ntl_WC2MB(req->awchPasswd));
	app->db->execute();
	app->db->execute("SELECT @acc_id, @result_code");
	app->db->fetch(); 

	res->wResultCode = app->db->getInt("@result_code");
	res->accountId = app->db->getInt("@acc_id");

	pMasterServer->SendLoginReq(res->abyAuthKey, res->accountId,0);

	string isEnabled = app->GetConfigFileEnabledMultipleServers();
	if (isEnabled == "true")
	{
		//Loading Max Server Files Configuration
		for(int i =0;i<app->GetConfigFileMaxServers();i++)
		{
			const char* CharIP = app->ServersConfig[i][0].c_str();
			const DWORD CharPort = atoi(app->ServersConfig[i][1].c_str());
			strcpy_s(res->aServerInfo[i].szCharacterServerIP, NTL_MAX_LENGTH_OF_IP, CharIP);
			res->aServerInfo[i].wCharacterServerPortForClient = CharPort;
			res->aServerInfo[i].dwLoad = 0;		
		}
		res->byServerInfoCount = app->GetConfigFileMaxServers();
		res->lastServerFarmId = INVALID_SERVERFARMID;
	}
	else
	{
		res->byServerInfoCount = 1;
		strcpy_s(res->aServerInfo[0].szCharacterServerIP, NTL_MAX_LENGTH_OF_IP, app->GetConfigFileExternalIP());
		res->dwAllowedFunctionForDeveloper = 0x01;
		res->aServerInfo[0].wCharacterServerPortForClient = 20300;
		res->aServerInfo[0].dwLoad = 0;
		res->lastServerFarmId = 0;
	}
	
	

	packet.SetPacketLen(sizeof(sAU_LOGIN_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
		if (NTL_SUCCESS != rc)
		{
			NTL_PRINT(PRINT_APP, "Failed to send packet %d(%s)", rc, NtlGetErrorMessage(rc));
		}
		else{
			NTL_PRINT(PRINT_APP, "User %S send to charserver", req->awchUserId);
		 }
}

//--------------------------------------------------------------------------------------//
//		Disconnect from Auth Server
//--------------------------------------------------------------------------------------//
void CClientSession::SendLoginDcReq(CNtlPacket * pPacket) 
{
	CNtlPacket packet(sizeof(sAU_LOGIN_DISCONNECT_RES));
	sAU_LOGIN_DISCONNECT_RES * res = (sAU_LOGIN_DISCONNECT_RES *)packet.GetPacketData();
	res->wOpCode = AU_LOGIN_DISCONNECT_RES;

	packet.SetPacketLen(sizeof(sAU_LOGIN_DISCONNECT_RES));
	int rc = g_pApp->Send(this->GetHandle(), &packet);
	this->Disconnect(false);
}
//-------------------------------------------------------------------------------------------------------------------------------------------------//
//------------------------------------------MASTER SERVER PACKET GENERATOR-------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------------------------------------//
//Lets notify the server will be begin
void CMasterSession::SendNotifyMasterServer()
{
	CNtlPacket packet(sizeof(sAM_HEARTBEAT));
	sAM_HEARTBEAT* res = (sAM_HEARTBEAT*)packet.GetPacketData();	
	res->wOpCode = AM_HEARTBEAT;
	packet.SetPacketLen(sizeof(sAM_HEARTBEAT));

	int rc = g_pApp->Send(this->GetHandle(), &packet);
	//int rc = this->m_MasterSocket->SendStream(packet->GetPacketData(), sizeof(packet), true);
	int error = WSAGetLastError();
}
//Sending a LogReq to Master Server
void CMasterSession::SendLoginReq(BYTE* pAuthKey,RwUInt32 accID,DWORD dwKey)
{
	CNtlPacket packet(sizeof(sAM_LOGIN_REQ));
	sAM_LOGIN_REQ* res = (sAM_LOGIN_REQ*)packet.GetPacketData();
	memcpy(res->abyAuthKey,pAuthKey,sizeof(NTL_MAX_SIZE_AUTH_KEY));
	res->accountId = accID;//We need.....split the function
	res->dwkey = dwKey;
	res->wOpCode = AM_LOGIN_REQ;
	packet.SetPacketLen(sizeof(sAM_LOGIN_REQ));

	int rc = g_pApp->Send(this->GetHandle(), &packet);
	//int rc = this->m_MasterSocket->SendStream(packet->GetPacketData(), sizeof(packet), true);
	int error = WSAGetLastError();
}
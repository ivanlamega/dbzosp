//-----------------------------------------------------------------------------------
//		Auth Server by Daneos @ Ragezone 
//-----------------------------------------------------------------------------------

#include "stdafx.h"
#include "AuthServer.h"

#include "NtlSfx.h"
#include "NtlFile.h"

#include "NtlPacketUA.h"
#include "NtlPacketAU.h"
#include "NtlPacketAM.h"
#include "NtlPacketMA.h"
#include "NtlResultCode.h"

#include <iostream>
#include <map>
#include <list>

using namespace std;


//-----------------------------------------------------------------------------------
CClientSession::~CClientSession()
{
	//NTL_PRINT(PRINT_APP, "CClientSession Destructor Called");
}


int CClientSession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );

	return NTL_SUCCESS;
}


void CClientSession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CAuthServer * app = (CAuthServer*) NtlSfxGetApp();
}

int CClientSession::OnDispatch(CNtlPacket * pPacket)
{
	CAuthServer * app = (CAuthServer*) NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch( pHeader->wOpCode )
	{
	case UA_LOGIN_REQ:
	{
		CClientSession::SendCharLogInReq(pPacket, app);
	}
		break;
	case UA_LOGIN_DISCONNECT_REQ:
	{
		CClientSession::SendLoginDcReq(pPacket);
	}
		break;

	default:
		return CNtlSession::OnDispatch( pPacket );
	}

	return NTL_SUCCESS;
}
/** 
*MasterServer Session must be called seprately from CClientSession.
*We need to know where to send the packets and this will do it.
**/


CMasterSession::~CMasterSession()
{
	//NTL_PRINT(PRINT_APP, "CClientSession Destructor Called");
}


int CMasterSession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();
	this->SendNotifyMasterServer();
	app->SetServerSession(this);

	return NTL_SUCCESS;
}


void CMasterSession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();
}

int CMasterSession::OnDispatch(CNtlPacket * pPacket)
{
	CAuthServer * app = (CAuthServer*)NtlSfxGetApp();

	//Just for test the receiving method - Kalisto
	/*sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);		
	switch (pHeader->wOpCode)
	{
	case MA_HEARTBEAT:
	{
		cout << "YES GETTING MA_ OPCODES!" << endl;
		//CMasterSession::SendCharLogInReq(pPacket, app);
	}
		break;
	
	default:
		return CNtlSession::OnDispatch(pPacket);
	}*/

	return NTL_SUCCESS;
}


//-----------------------------------------------------------------------------------
//		AuthServerMain
//-----------------------------------------------------------------------------------
int AuthServerMain(int argc, _TCHAR* argv[])
{
	CAuthServer app;
	CNtlFileStream traceFileStream;
	CMasterSession mSession;
	// LOG FILE
	int rc = traceFileStream.Create("authlog");
	if (NTL_SUCCESS != rc)
	{
		printf("log file CreateFile error %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}

	// CHECK INI FILE AND START PROGRAM
	NtlSetPrintStream(traceFileStream.GetFilePtr());
	NtlSetPrintFlag(PRINT_APP | PRINT_SYSTEM);

	rc = app.Create(argc, argv, ".\\Server.ini");
	if (NTL_SUCCESS != rc)
	{
		NTL_PRINT(PRINT_APP, "Server Application Create Fail %d(%s)", rc, NtlGetErrorMessage(rc));
		return rc;
	}

	// CONNECT TO MYSQL
	app.db = new MySQLConnWrapper;
	app.db->setConfig(app.GetConfigFileHost(), app.GetConfigFileUser(), app.GetConfigFilePassword(), app.GetConfigFileDatabase());
	try
	{
		app.db->connect();
		printf("Connected to database server.\n\r");
	}
	catch (exception e)
	{
		printf("couldn't connect to database server ErrID:%s\n\r", e.what());
	}
	try
	{
		app.db->switchDb(app.GetConfigFileDatabase());
	}
	catch (exception e)
	{
		printf("Couldn't switch database to %s Error:%s\n\r", app.GetConfigFileDatabase(), e.what());
	}

	app.Start();
	Sleep(500);
	std::cout << "\n\n" << std::endl;
	std::cout << "\t  ____                              ____        _ _ " << std::endl;
	std::cout << "\t |  _ \\ _ __ __ _  __ _  ___  _ __ | __ )  __ _| | |" << std::endl;
	std::cout << "\t | | | | '__/ _` |/ _` |/ _ \\| '_ \\|  _ \\ / _` | | |" << std::endl;
	std::cout << "\t | |_| | | | (_| | (_| | (_) | | | | |_) | (_| | | |" << std::endl;
	std::cout << "\t |____/|_|  \\__,_|\\__, |\\___/|_| |_|____/ \\__,_|_|_|" << std::endl;
	std::cout << "\t                  |___/                             " << std::endl;
	std::cout << "\t______           AKCore :O 2014					______\n\n" << std::endl;
	cout << endl;
	cout << endl;
	cout << endl;
	cout << endl;
	cout << "YOU HAVE 3 SECONDS TO START MASTER SERVER OR THIS SERVER WILL CRASH!" << endl;
	Sleep(5000);
	//app.CheckMasterServer();

	//Send a notification to Master Server	
	app.WaitForTerminate();	
	return 0;
}

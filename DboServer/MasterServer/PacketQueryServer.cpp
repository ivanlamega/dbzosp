#include "stdafx.h"
#include "NtlSfx.h"
/*Packets Needed*/
#include <NtlPacketAll.h>
#include "NtlResultCode.h"

#include "MasterServer.h"


/**--------------------------------------------------------------------------------------//
//								Session Handling										//
//--------------------------------------------------------------------------------------/*/

CQuerySession::~CQuerySession()
{
	//NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");
}


int CQuerySession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	cout << "QueryServer  Connected" << endl;
	return NTL_SUCCESS;
}


void CQuerySession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
}

int CQuerySession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch (pHeader->wOpCode)
	{
	case QM_HEARTBEAT:
		//CQuerySession::ReceiveHeartBeat(app);
		break;


	default:
		return CNtlSession::OnDispatch(pPacket);
	}

	return NTL_SUCCESS;
}


/**--------------------------------------------------------------------------------------//
//							Packets Handling Starts Here								//
//--------------------------------------------------------------------------------------*/


#pragma once

#include "NtlSfx.h"
#include "NtlPacketEncoder_RandKey.h"
#include "mysqlconn_wrapper.h"

/*Include packets needed*/
#include <NtlPacketAll.h>



#
enum APP_LOG
{
	PRINT_APP = 2,
};
enum MASTER_SESSION
{
	SESSION_AUTH_SERVER,
	SESSION_QUERY_SERVER,
	SESSION_CHAR_SERVER,
	SESSION_COMMUNITY_SERVER,
	SESSION_NPC_SERVER, //NPC server handles both Mobs and NPCs we don't need a seprate one for MOBs - SanGawku
	SESSION_GAME_SERVER,
	SESSION_SERVER_ACTIVE,
};
struct sSERVERCONFIG
{
	//Auth Server
	CNtlString		strAuthAcceptAddr;
	WORD			wAuthAcceptPort;
	CNtlString		strAuthConnectAddr;
	WORD			wAuthConnectPort;
	
	//Database Server
	CNtlString		strQueryAcceptAddr;
	WORD			wQueryAcceptPort;
	CNtlString		strQueryConnectAddr;
	WORD			wQueryConnectPort;
	
	//Char Server
	CNtlString		strCharAcceptAddr;	//Each server requires a Accept and Connect.
	WORD			wCharAcceptPort;
	CNtlString		strCharConnectAddr;
	WORD			wCharConnectPort;

	//Community Server
	CNtlString		strCommunityAcceptAddr;
	WORD			wCommunityAcceptPort;
	CNtlString		strCommunityConnectAddr;
	WORD			wCommunityConnectPort;

	//NPC Server
	CNtlString		strNPCAcceptAddr;
	WORD			wNPCAcceptPort;
	CNtlString		strNPCConnectAddr;
	WORD			wNPCConnectPort;

	//Game Server
	CNtlString		strGameAcceptAddr;
	WORD			wGameAcceptPort;
	CNtlString		strGameConnectAddr;
	WORD			wGameConnectPort;

	CNtlString		ExternalIP;
	CNtlString		Host;
	CNtlString		User;
	CNtlString		Password;
	CNtlString		Database;
};

class CMasterServer;

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CAuthSession : public CNtlSession
{
public:

	CAuthSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_AUTH_SERVER
		 )
	{
		SetControlFlag( CONTROL_FLAG_USE_SEND_QUEUE );

		if( bAliveCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_ALIVE );
		}
		if( bOpcodeCheck )
		{
			SetControlFlag( CONTROL_FLAG_CHECK_OPCODE );
		}

		SetPacketEncoder( &m_packetEncoder );
	}

	~CAuthSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	// Packet functions
	void						ReceiveAuthNotify(CNtlPacket * pPacket, CMasterServer * app);
	void						SendLoadReport(CNtlPacket* pPacket, CMasterServer * app);
	void						SendTurnOffAllNfy(CNtlPacket* pPacket, CMasterServer * app);
	void						SendLoginRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendLogoutRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendMoveRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendKickoutRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendPlayerInfoRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendPingRes(CNtlPacket* pPacket, CMasterServer * app);
	void						SendLoginDcReq(CNtlPacket * pPacket);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};

//---------------------------------------------------------------------------------------------------//
//										Query Session Handler										//
//-------------------------------------------------------------------------------------------------//


class CQuerySession : public CNtlSession
{
public:

	CQuerySession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_AUTH_SERVER
		)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CQuerySession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	// Packet functions
	void						ReceiveHeartBeat(CMasterServer* app);

	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};


//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CCharSession : public CNtlSession
{
public:

	CCharSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CCharSession(SESSION_CHAR_SERVER
		)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CCharSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);
	// Packet functions
	void						ReceiveHeartBeat(CMasterServer* app);
	void						SendCharExitRes(CNtlPacket* pPacket, CMasterServer* app);
	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};



//---------------------------------------------------------------------------------------------------//
//										Community Session Handler									//
//-------------------------------------------------------------------------------------------------//

class CCommunitySession : public CNtlSession
{
public:

	CCommunitySession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_AUTH_SERVER
		)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CCommunitySession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	// Packet functions
	void						ReceiveHeartBeat(CMasterServer* app);

	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};


//---------------------------------------------------------------------------------------------------//
//										NPC Session Handler											//
//-------------------------------------------------------------------------------------------------//

class CNPCSession : public CNtlSession
{
public:

	CNPCSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_AUTH_SERVER
		)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CNPCSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	// Packet functions
	void						ReceiveHeartBeat(CMasterServer* app);

	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};

//---------------------------------------------------------------------------------------------------//
//										Game Session Handler										//
//-------------------------------------------------------------------------------------------------//

class CGameSession : public CNtlSession
{
public:

	CGameSession(bool bAliveCheck = false, bool bOpcodeCheck = false)
		:CNtlSession(SESSION_AUTH_SERVER
		)
	{
		SetControlFlag(CONTROL_FLAG_USE_SEND_QUEUE);

		if (bAliveCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_ALIVE);
		}
		if (bOpcodeCheck)
		{
			SetControlFlag(CONTROL_FLAG_CHECK_OPCODE);
		}

		SetPacketEncoder(&m_packetEncoder);
	}

	~CGameSession();

public:
	int							OnAccept();
	void						OnClose();
	int							OnDispatch(CNtlPacket * pPacket);

	// Packet functions
	void						ReceiveHeartBeat(CMasterServer* app);

	// End Packet functions
private:
	CNtlPacketEncoder_RandKey	m_packetEncoder;

};


//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CMasterSessionFactory : public CNtlSessionFactory
{
public:

	CNtlSession * CreateSession(SESSIONTYPE sessionType)
	{
		CNtlSession * pSession = NULL;
		switch( sessionType )
		{
		case SESSION_AUTH_SERVER: //Setup for new sessions. For each server requires a session
			{
				pSession = new CAuthSession;
			}
			break;

		case SESSION_QUERY_SERVER: //Setup for new sessions. For each server requires a session
		{
			pSession = new CQuerySession;
		}
			break;

		
		case SESSION_CHAR_SERVER : //Setup for new Char session. For each server requires a session
		{
			pSession = new CCharSession;
		}
			
			break;
		
		case SESSION_COMMUNITY_SERVER: //Setup for new sessions. For each server requires a session
		{
			pSession = new CCommunitySession;
		}
			break;
		case SESSION_NPC_SERVER: //Setup for new sessions. For each server requires a session
		{
			pSession = new CNPCSession;
		}
			break;

		case SESSION_GAME_SERVER: //Setup for new sessions. For each server requires a session
		{
			pSession = new CGameSession;
		}
			break;

		default:
			break;
		}

		return pSession;
	}
};

//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------//

class CMasterServer : public CNtlServerApp
{
public:
	const char*		GetConfigFileHost()
	{
		return m_config.Host.c_str();
	}
	const char*		GetConfigFileUser()
	{
		return m_config.User.c_str();
	}
	const char*		GetConfigFilePassword()
	{
		return m_config.Password.c_str();
	}
	const char*		GetConfigFileDatabase()
	{
		return m_config.Database.c_str();
	}
	//For Multiple Server - Luiz45
	const string GetConfigFileEnabledMultipleServers()
	{
		return EnableMultipleServers.GetString();
	}
	const DWORD GetConfigFileMaxServers()
	{
		return MAX_NUMOF_SERVER;
	}

	const char*		GetConfigFileExternalIP()
	{
		std::cout << m_config.ExternalIP.c_str() << std::endl;
		return m_config.ExternalIP.c_str();
	}
	int	OnInitApp()
	{
		m_nMaxSessionCount = MAX_NUMOF_SESSION;

		m_pSessionFactory =  new CMasterSessionFactory;
		if( NULL == m_pSessionFactory )
		{
			return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
		}

		return NTL_SUCCESS;
	}

	int	OnCreate()
	{
		int rc = NTL_SUCCESS;
		rc = m_AuthAcceptor.Create(m_config.strAuthAcceptAddr.c_str(), m_config.wAuthAcceptPort, SESSION_AUTH_SERVER,
										MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT ); //Prepares a Socket for the auth server to connect to
		if ( NTL_SUCCESS != rc )
		{
			return rc;
		}

		rc = m_AuthConnector.Create(m_config.strAuthConnectAddr.c_str(), m_config.wAuthConnectPort, SESSION_AUTH_SERVER); // Prepares to connect to the auth Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}


		rc = m_QueryAcceptor.Create(m_config.strQueryAcceptAddr.c_str(), m_config.wQueryAcceptPort, SESSION_QUERY_SERVER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT); //Prepares a Socket for the Query server to connect to
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_QueryConnector.Create(m_config.strQueryConnectAddr.c_str(), m_config.wQueryConnectPort, SESSION_QUERY_SERVER); // Prepares to connect to the Query Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}


		rc = m_CharAcceptor.Create(m_config.strCharAcceptAddr.c_str(), m_config.wCharAcceptPort, SESSION_CHAR_SERVER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT); //Prepares a Socket for the auth server to connect to
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_CharConnector.Create(m_config.strCharConnectAddr.c_str(), m_config.wCharConnectPort, SESSION_CHAR_SERVER); // Prepares to connect to the auth Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_CommunityAcceptor.Create(m_config.strCommunityAcceptAddr.c_str(), m_config.wCommunityAcceptPort, SESSION_COMMUNITY_SERVER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT); //Prepares a Socket for the Community server to connect to
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_CommunityConnector.Create(m_config.strCommunityConnectAddr.c_str(), m_config.wCommunityConnectPort, SESSION_COMMUNITY_SERVER); // Prepares to connect to the Community Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_NPCAcceptor.Create(m_config.strNPCAcceptAddr.c_str(), m_config.wNPCAcceptPort, SESSION_NPC_SERVER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT); //Prepares a Socket for the NPC server to connect to
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_NPCConnector.Create(m_config.strNPCConnectAddr.c_str(), m_config.wNPCConnectPort, SESSION_NPC_SERVER); // Prepares to connect to the NPC Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}


		rc = m_GameAcceptor.Create(m_config.strGameAcceptAddr.c_str(), m_config.wGameAcceptPort, SESSION_GAME_SERVER,
			MAX_NUMOF_GAME_CLIENT, 5, 2, MAX_NUMOF_GAME_CLIENT); //Prepares a Socket for the Game server to connect to
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_GameConnector.Create(m_config.strGameConnectAddr.c_str(), m_config.wGameConnectPort, SESSION_GAME_SERVER); // Prepares to connect to the Game Server with SESSION_*

		if (NTL_SUCCESS != rc)
		{
			return rc;
		}



		rc = m_network.Associate( &m_AuthAcceptor, true ); //Assiocates AuthServer with MasterServer
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}

		rc = m_network.Associate(&m_AuthConnector, true); //Connects MasterServer with Auth Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_QueryAcceptor, true); //Assiocates QueryServer with MasterServer
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_QueryConnector, true); //Connects MasterServer with Query Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_CharAcceptor, true); //Assiocates CharServer with MasterServer
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_CharConnector, true); //Connects MasterServer with Char Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_CommunityAcceptor, true); //Assiocates CommunityServer with MasterServer
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_CommunityConnector, true); //Connects MasterServer with Community Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_NPCAcceptor, true); //Assiocates NPCServer with MasterServer
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_NPCConnector, true); //Connects MasterServer with NPC Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_GameAcceptor, true); //Assiocates GameServer with MasterServer
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}

		rc = m_network.Associate(&m_GameConnector, true); //Connects MasterServer with Game Server
		if (NTL_SUCCESS != rc)
		{
			return rc;
		}


		return NTL_SUCCESS;

	}

	void	OnDestroy()
	{
	}

	int	OnCommandArgument(int argc, _TCHAR* argv[])
	{
		return NTL_SUCCESS;
	}
	//Just for Universal Calling
	void SetAuthSession(CAuthSession* pAuthSession)
	{
		this->m_AuthSession = pAuthSession;
	}
	void SetQuerySession(CQuerySession* pQuerySession)
	{
		this->m_QuerySession = pQuerySession;
	}
	void SetCharSession(CCharSession* pCharSession)
	{
		this->m_CharSession = pCharSession;
	}
	void SetCommunitySession(CCommunitySession* pCommunitySession)
	{
		this->m_CommunitySession = pCommunitySession;
	}
	void SetNPCSession(CNPCSession* pNPCSession)
	{
		this->m_NPCSession = pNPCSession;
	}
	void SetGameSession(CGameSession* pGameSession)
	{
		this->m_GameSession = pGameSession;
	}
	CAuthSession* GetAuthSession()
	{
		return this->m_AuthSession;
	}
	CQuerySession* GetQuerySession()
	{
		return this->m_QuerySession;
	}
	CCharSession* GetCharSession()
	{
		return this->m_CharSession;
	}
	CCommunitySession* GetCommunitySession()
	{
		return this->m_CommunitySession;
	}
	CNPCSession* GetNPCSession()
	{
		return this->m_NPCSession;
	}
	CGameSession* GetGameSession()
	{
		return this->m_GameSession;
	}
	//
	int	OnConfiguration(const char * lpszConfigFile)
	{
		CNtlIniFile file;

		int rc = file.Create( lpszConfigFile );
		if( NTL_SUCCESS != rc )
		{
			return rc;
		}
		if( !file.Read("IPAddress", "Address", m_config.ExternalIP) )
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		//For multiple servers - Luiz45
		if (!file.Read("ServerOptions", "EnableMultipleServers", EnableMultipleServers))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("ServerOptions", "MaxServerAllowed", MAX_NUMOF_SERVER))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (EnableMultipleServers.GetString() == "true")
		{
			for (int i = 0; i < MAX_NUMOF_SERVER; i++)
			{
				string strNm = "Char Server" + std::to_string(i);
				if (!file.Read(strNm.c_str(), "Address", ServersConfig[i][0]))
				{
					return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
				if (!file.Read(strNm.c_str(), "Port", ServersConfig[i][1]))
				{
					return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
				}
			}
		}
		if (!file.Read("Auth Server", "MasterAddress", m_config.strAuthConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Auth Server", "MasterPort", m_config.wAuthConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}

		if (!file.Read("Query Server", "MasterAddress", m_config.strQueryConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Query Server", "MasterPort", m_config.wQueryConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}

		if (!file.Read("Char Server", "MasterAddress", m_config.strCharConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Char Server", "MasterPort", m_config.wCharConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Community Server", "MasterAddress", m_config.strCommunityConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Community Server", "MasterPort", m_config.wCommunityConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("NPC Server", "MasterAddress", m_config.strNPCConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("NPC Server", "MasterPort", m_config.wNPCConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Game Server", "MasterAddress", m_config.strGameConnectAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Game Server", "MasterPort", m_config.wGameConnectPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "AuthAddress", m_config.strAuthAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "AuthPort", m_config.wAuthAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "QueryAddress", m_config.strQueryAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "QueryPort", m_config.wQueryAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "CharAddress", m_config.strCharAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "CharPort", m_config.wCharAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "CommunityAddress", m_config.strCommunityAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "CommunityPort", m_config.wCommunityAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "NPCAddress", m_config.strNPCAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "NPCPort", m_config.wNPCAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "GameAddress", m_config.strGameAcceptAddr))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if (!file.Read("Master Server", "GamePort", m_config.wGameAcceptPort))
		{
			return NTL_ERR_SYS_CONFIG_FILE_READ_FAIL;
		}
		if( !file.Read("DATABASE", "Host",  m_config.Host) )
		{
			return NTL_ERR_DBC_HANDLE_ALREADY_ALLOCATED;
		}
		if( !file.Read("DATABASE", "User",  m_config.User) )
		{
			return NTL_ERR_SYS_MEMORY_ALLOC_FAIL;
		}
		if( !file.Read("DATABASE", "Password",  m_config.Password) )
		{
			return NTL_ERR_SYS_LOG_SYSTEM_INITIALIZE_FAIL;
		}
		if( !file.Read("DATABASE", "Db",  m_config.Database) )
		{
			return NTL_ERR_DBC_CONNECTION_CONNECT_FAIL;
		}
		return NTL_SUCCESS;
	}

	int	OnAppStart()
	{
		return NTL_SUCCESS;
	}

	void	Run()
	{
		DWORD dwTickCur, dwTickOld = ::GetTickCount();

		while( IsRunnable() )
		{		
			dwTickCur = ::GetTickCount();
			if( dwTickCur - dwTickOld >= 10000 )
			{
			//	NTL_PRINT(PRINT_APP, "Master Server Run()");
				dwTickOld = dwTickCur;
			}
			Sleep(2);
		}
	}

private:
	CNtlAcceptor				m_AuthAcceptor;	 //Use to connect to auth server. - Kalisto
	CNtlConnector				m_AuthConnector; //Each server requires both an acceptor and a connector - kalisto

	CNtlAcceptor				m_QueryAcceptor;	 //Use to connect to Query server. - Kalisto		
	CNtlConnector				m_QueryConnector;   //Each server requires both an acceptor and a connector - kalisto 

	CNtlAcceptor				m_CharAcceptor;	 //Use to connect to Char server. - Kalisto		
	CNtlConnector				m_CharConnector; //Each server requires both an acceptor and a connector - kalisto 

	CNtlAcceptor				m_CommunityAcceptor;	 //Use to connect to Query server. - Kalisto		
	CNtlConnector				m_CommunityConnector;   //Each server requires both an acceptor and a connector - kalisto 

	CNtlAcceptor				m_NPCAcceptor;	 //Use to connect to Query server. - Kalisto		
	CNtlConnector				m_NPCConnector;   //Each server requires both an acceptor and a connector - kalisto 

	CNtlAcceptor				m_GameAcceptor;	 //Use to connect to Query server. - Kalisto		
	CNtlConnector				m_GameConnector;   //Each server requires both an acceptor and a connector - kalisto 



	//Store the session for universall calling - Luiz 45
	CAuthSession*				m_AuthSession;
	CQuerySession*				m_QuerySession;
	CCharSession*				m_CharSession;
	CCommunitySession*			m_CommunitySession;
	CNPCSession*				m_NPCSession;
	CGameSession*				m_GameSession;


	CNtlLog  					m_log;
	sSERVERCONFIG				m_config;
	DWORD						MAX_NUMOF_SERVER = 1;//This will be defined how many servers we can load
	CNtlString					EnableMultipleServers;//Added for enabling multiple server - Luiz45
	DWORD						MAX_NUMOF_GAME_CLIENT = 7;
	DWORD						MAX_NUMOF_SESSION = MAX_NUMOF_GAME_CLIENT + MAX_NUMOF_SERVER;
public:
	MySQLConnWrapper *			db;
	CNtlString					ServersConfig[99][2];//For Config of multiple server 0->{Ip,Port} - Luiz45
};
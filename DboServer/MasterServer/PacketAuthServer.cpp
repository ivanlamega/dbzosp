#include "stdafx.h"
#include "NtlSfx.h"
/*Packets Needed*/
#include <NtlPacketAll.h>
#include "NtlResultCode.h"

#include "MasterServer.h"


/**--------------------------------------------------------------------------------------//
//								Session Handling										//
//--------------------------------------------------------------------------------------/*/

CAuthSession::~CAuthSession()
{
	//NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");
}


int CAuthSession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	cout << "AuthServer Connected" << endl;
	return NTL_SUCCESS;
}


void CAuthSession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
}

int CAuthSession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch (pHeader->wOpCode)
	{
	case AM_NOTIFY_SERVER_BEGIN:
	{
		cout << "~~~ AM_NOTIFY_SERVER_BEGIN Received ~~~" << endl;
	}
		break;
	case AM_HEARTBEAT:
	{
		CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_LOGIN_REQ:
	{
		cout << "~~~ AM_LOGIN_REQ Received ~~~" << endl;
		//CAuthSession::SendLoginRes(pPacket, app);
	}
		break;
	case AM_LOGOUT_REQ:
	{
		cout << "~~~ AM_LOGOUT_REQ Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_MOVE_REQ:
	{
		cout << "~~~ AM_MOVE_REQ Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_ON_PLAYER_INFO:
	{
		cout << "~~~ AM_ON_PLAYER_INFO Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_PING_REQ:
	{
		cout << "~~~ AM_PING_REQ Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_REPORT_LOAD:
	{
		cout << "~~~ AM_REPORT_LOAD Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;
	case AM_SERVER_CONTROL_TURN_OFF_ALL_NFY:
	{
		cout << "~~~ AM_SERVER_CONTROL_TURN_OFF_ALL_NFY Received ~~~" << endl;
		//CAuthSession::ReceiveAuthNotify(pPacket, app);
	}
		break;

	default:
		return CNtlSession::OnDispatch(pPacket);
	}

	return NTL_SUCCESS;
}

/**--------------------------------------------------------------------------------------//
//							Packets Handling Starts Here								//
//--------------------------------------------------------------------------------------*/



/**--------------------------------------------------------------------------------------//
//		Disconnect from Master Server
//--------------------------------------------------------------------------------------*/

void CAuthSession::ReceiveAuthNotify(CNtlPacket * pPacket, CMasterServer * app)
{
	cout << "Packet Received!" << endl;
}
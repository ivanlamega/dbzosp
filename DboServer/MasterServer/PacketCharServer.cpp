#include "stdafx.h"
#include "NtlSfx.h"
/*Packets Needed*/
#include <NtlPacketAll.h>
#include "NtlResultCode.h"

#include "MasterServer.h"


/**--------------------------------------------------------------------------------------//
//								Session Handling										//
//--------------------------------------------------------------------------------------/*/

CCharSession::~CCharSession()
{
	//NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");
}


int CCharSession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	cout << "CharServer  Connected" << endl;
	return NTL_SUCCESS;
}


void CCharSession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
}

int CCharSession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch (pHeader->wOpCode)
	{
	case CM_HEARTBEAT:
		//CCharSession::ReceiveHeartBeat(app);
		break;
	case CM_CHARACTER_EXIT_REQ:
		//CCharSession::SendCharExitRes(pPacket, app);
		break;


	default:
		return CNtlSession::OnDispatch(pPacket);
	}

	return NTL_SUCCESS;
}


/**--------------------------------------------------------------------------------------//
//							Packets Handling Starts Here								//
//--------------------------------------------------------------------------------------*/


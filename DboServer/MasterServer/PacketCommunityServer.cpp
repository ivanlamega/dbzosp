#include "stdafx.h"
#include "NtlSfx.h"
/*Packets Needed*/
#include <NtlPacketAll.h>
#include "NtlResultCode.h"

#include "MasterServer.h"


/**--------------------------------------------------------------------------------------//
//								Session Handling										//
//--------------------------------------------------------------------------------------/*/

CCommunitySession::~CCommunitySession()
{
	//NTL_PRINT(PRINT_APP, "CAuthSession Destructor Called");
}


int CCommunitySession::OnAccept()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	cout << "CommunityServer  Connected" << endl;
	return NTL_SUCCESS;
}


void CCommunitySession::OnClose()
{
	//NTL_PRINT( PRINT_APP, "%s", __FUNCTION__ );
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();
}

int CCommunitySession::OnDispatch(CNtlPacket * pPacket)
{
	CMasterServer * app = (CMasterServer*)NtlSfxGetApp();

	sNTLPACKETHEADER * pHeader = (sNTLPACKETHEADER *)pPacket->GetPacketData();
	//printf("~~~ opcode %i received ~~~ \n", pHeader->wOpCode);
	switch (pHeader->wOpCode)
	{
	case TM_HEARTBEAT:
		//CCommunitySession::ReceiveHeartBeat(app);
		break;


	default:
		return CNtlSession::OnDispatch(pPacket);
	}

	return NTL_SUCCESS;
}


/**--------------------------------------------------------------------------------------//
//							Packets Handling Starts Here								//
//--------------------------------------------------------------------------------------*/


# Dragonball Online
Client Source code 

# Working with DBO
Dragonball Online source code was written on a machine using the Korean Codepage.

As such until we can convert all the comments into English you have a couple options.

- 'Microsoft AppLocale and run visual studio in Korean'
- 'Change windows locale to Korean'

Once we change the codepage of all the code you can happily just run it in your native codepage.

# Branches
The HEAD branch is the stable branch

If you would like to make changes please push your commits to the experimental branch for testing.

# Building

You need only 3 programs to build the client. Some of the tools require additional resources that are currently outside the scope of this readme

- 'Microsoft Visual Studio 2013 Ultimate'
- 'Multibyte MFC Library for Visual Studio 2013'
- 'DirectX SDX(Feb. 2010)'

Build the client only in debug mode until I update this readme with more testing done.